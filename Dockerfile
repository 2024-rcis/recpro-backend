FROM openjdk:17
# VOLUME [ "/tmp" ]

COPY target/classes/ontology /ontology/
COPY target/recpro-backend-1.0.0.jar recpro.jar

ENTRYPOINT [ "java", "-jar", "/recpro.jar" ]
