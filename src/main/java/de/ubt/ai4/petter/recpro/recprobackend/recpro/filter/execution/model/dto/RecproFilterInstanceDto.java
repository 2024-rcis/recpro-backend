package de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.model.dto;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class RecproFilterInstanceDto {
    private FilterInstance filterInstance;
    private Filter filter;
}
