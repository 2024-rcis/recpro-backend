package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecproRatingInstanceDto {
    private Long id;
}
