package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.tasklist;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.tasklist.TasklistService;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.BaseFilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.KnowledgeBasedFilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResult;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResultItem;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.base.FilterModelingService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTaskDto;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTasklistDto;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.task.RecproTaskDtoCreator;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.RecproFilterExecutionService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.model.dto.RecproFilterInstanceDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;
import static java.util.Comparator.*;

@Service
@AllArgsConstructor
public class RecproTasklistService {
    private TasklistService tasklistService;
    private RecproTaskDtoCreator taskDtoCreator;
    private RecproFilterExecutionService filterExecutionService;
    private FilterModelingService recproFilterModelingServiceImpl;


    public RecproTasklistDto getOrderedTasklist(String assigneeId) {
        Tasklist tasklist = tasklistService.save(tasklistService.getTasklist(assigneeId));
        FilterInstance kbfi = filterExecutionService.getLatestFilterInstance(null, tasklist, FilterType.KNOWLEDGE_BASED);
        FilterInstance bfi = filterExecutionService.getLatestFilterInstance(null, tasklist, FilterType.BASE);
        FilterInstance fi = filterExecutionService.createFilter(null, tasklist, null);

       return this.filter(tasklist, kbfi, bfi, fi);
    }

    private RecproTasklistDto filter(Tasklist tasklist, FilterInstance kbfi, FilterInstance bfi, FilterInstance mfi) {
        RecproFilterInstanceDto kbfidto = this.callKnowledgeBasedFilter(kbfi);
        RecproFilterInstanceDto fidto = this.callMainFilter(mfi);

        FilterResult filterResult = kbfidto.getFilterInstance().getResult().copy();

        for (FilterResultItem item : filterResult.getItems()) {
            FilterResultItem fidtoItem = fidto.getFilterInstance().getResult().getResultItemByTaskId(item.getTaskId());
            item.setValue(fidtoItem.getValue());
        }

        bfi.getResult().setItems(filterResult.getItems());

        RecproFilterInstanceDto bfidto = this.callBaseFilter(bfi);

        List<RecproTaskDto> tasks = taskDtoCreator.fromRecproTask(tasklist.getTasks(), bfidto.getFilterInstance().getResult().getItems());

        return RecproTasklistDtoCreator.createTasklistDto(tasklist, tasks, kbfidto, bfidto, fidto);
    }

    private RecproFilterInstanceDto callKnowledgeBasedFilter(FilterInstance kbfi) {
        Filter filter = recproFilterModelingServiceImpl.getById(kbfi.getFilterId());
//        RestTemplate template = new RestTemplate();
//        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(filter.getFilterUrl());
//        Objects.requireNonNull(template.postForObject(builder.toUriString(), kbfi, KnowledgeBasedFilterInstance.class));
//        kbfi = filterExecutionService.save(kbfi);
        return new RecproFilterInstanceDto(kbfi, filter);
    }

    private RecproFilterInstanceDto callBaseFilter(FilterInstance bfi) {
        Filter filter = recproFilterModelingServiceImpl.getById(bfi.getFilterId());

        List<Task> sortedList = new ArrayList<>();
        switch (((BaseFilterInstance) bfi).getTasklistOrder()) {
            case CREATE_DATE -> sortedList = bfi.getTasklist().getTasks().stream().sorted(nullsLast(comparing(Task::getCreateDate, ((BaseFilterInstance) bfi).isAscending() ? nullsLast(reverseOrder()) : nullsLast(naturalOrder())))).toList();
            case DUE_DATE -> sortedList = bfi.getTasklist().getTasks().stream().sorted(nullsLast(comparing(Task::getDueDate, ((BaseFilterInstance) bfi).isAscending() ? nullsLast(reverseOrder()) : nullsLast(naturalOrder())))).toList();
            case PRIORITY -> sortedList = bfi.getTasklist().getTasks().stream().sorted(nullsLast(comparing(Task::getPriority, ((BaseFilterInstance) bfi).isAscending() ? nullsLast(reverseOrder()) : nullsLast(naturalOrder())))).toList();
            case LIKELINESS -> sortedList = bfi.getTasklist().getTasks().stream().sorted(nullsLast(comparing(Task::getLikeliness, ((BaseFilterInstance) bfi).isAscending() ? nullsLast(reverseOrder()) : nullsLast(naturalOrder())))).toList(); //TODO: Order by likeliness!
        }

        List<Task> finalSortedList = sortedList;

        IntStream.range(0, sortedList.size()).forEach(i -> {
            Task t = finalSortedList.get(i);
            FilterResultItem resItem =  bfi.getResult().getResultItemByTaskId(t.getId());
            resItem.setPosition(i + 1);
        });
        return new RecproFilterInstanceDto(filterExecutionService.save(bfi), filter);
    }

    private RecproFilterInstanceDto callMainFilter(FilterInstance fi) {
        Filter filter = recproFilterModelingServiceImpl.getById(fi.getFilterId());
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(filter.getFilterUrl());
        fi = Objects.requireNonNull(template.postForObject(builder.toUriString(), fi, FilterInstance.class));

        return new RecproFilterInstanceDto(filterExecutionService.save(fi), filter);
    }

    public RecproTasklistDto getOrderedTasklist(FilterInstance filterInstance) {
        Tasklist tasklist = tasklistService.save(tasklistService.getTasklist(filterInstance.getAssigneeId())); // Tasklist before filtering!

        FilterInstance kbfi = filterExecutionService.getLatestFilterInstance(filterInstance, tasklist, FilterType.KNOWLEDGE_BASED);
        FilterInstance bfi = filterExecutionService.getLatestFilterInstance(filterInstance, tasklist, FilterType.BASE);
        FilterInstance fi = filterExecutionService.createFilter(null, tasklist, null);

        return this.filter(tasklist, kbfi, bfi, fi);
    }


    public Tasklist getById(Long tasklistId) {
        return tasklistService.getById(tasklistId);
    }
}
