package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.processInstance;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Process;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.ProcessInstance;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.processInstance.ProcessInstanceService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.modelingService.OntologyModelingBpmProcessService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.attribute.execution.RecproAttributeInstanceDtoCreator;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproProcessInstanceDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class ProcessInstanceDtoCreator {
    private OntologyModelingBpmProcessService ontologyProcessService;
    private ProcessInstanceService processInstanceService;
    private RecproAttributeInstanceDtoCreator attributeDtoCreator;
    public RecproProcessInstanceDto fromProcessInstanceId(String processInstanceId, String processId) {
        ProcessInstance processInstance = processInstanceService.getByProcessInstanceId(processInstanceId, processId);
        Process process = ontologyProcessService.getById(processId);

        RecproProcessInstanceDto result = new RecproProcessInstanceDto();
        result.setId(processInstance.getId());
        result.setBpmsProcessInstanceId(processInstance.getBpmsProcessInstanceId());
        result.setRecproElementId(processInstance.getRecproElementId());
        result.setRecproElementInstanceId(processInstance.getRecproElementId());
        result.setProcessName(process.getName());

        result.setAttributes(attributeDtoCreator.fromRecproAttribute(process.getAttributes(), processInstance.getRecproElementId(), processInstance.getRecproElementInstanceId(), processInstance.getRecproElementInstanceId()));

        return result;
    }

}
