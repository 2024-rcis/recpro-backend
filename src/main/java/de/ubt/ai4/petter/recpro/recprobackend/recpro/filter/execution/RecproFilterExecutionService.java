package de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import de.ubt.ai4.petter.recpro.lib.filter.filterexecution.service.IFilterExecutionService;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.creatorServices.RecproFilterInstanceDtoCreator;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.model.dto.RecproFilterInstanceDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class RecproFilterExecutionService {

    private RecproFilterInstanceDtoCreator filterInstanceDtoCreator;
    private IFilterExecutionService filterExecutionServiceImpl;

    public RecproFilterInstanceDto getNewByFilterId(String filterId, Long tasklistId, String assigneeId, FilterType filterType) {
        return filterInstanceDtoCreator.fromRecproFilterInstance(filterExecutionServiceImpl.create(filterId, tasklistId, assigneeId, filterType));
    }

    public RecproFilterInstanceDto getNewByFilter(Filter filter, Long tasklistId, String assigneeId) {
        return filterInstanceDtoCreator.fromRecproFilter(filter, tasklistId, assigneeId);
    }

    public FilterInstance getByTasklistId(Long tasklistId, String assigneeId, FilterType filterType) {
        return filterExecutionServiceImpl.getByTasklistIdAndFilterType(tasklistId, assigneeId, filterType);
    }

    public FilterInstance getLatestFilterInstance(FilterInstance instance, Tasklist tasklist, FilterType type) {
        if (instance != null && instance.getFilterType().equals(type)) {
            FilterInstance fiInitial = instance.copy();
            fiInitial.setTasklist(tasklist);
            return fiInitial;
        } else {
            return this.getLatestFilterInstance(tasklist.getId(), tasklist.getAssigneeId(), type, tasklist);
        }
    }

    private FilterInstance getLatestFilterInstance(Long tasklistId, String assigneeId, FilterType filterType, Tasklist tasklist) {
        FilterInstance instance = new FilterInstance(assigneeId, tasklistId, filterType);
        instance.setTasklist(tasklist);
        FilterInstance latest = filterExecutionServiceImpl.getLatestByFilterInstance(instance);
        latest.setTasklistId(tasklistId);
        latest.setAssigneeId(assigneeId);
        return latest;
    }

    public FilterInstance createFilter(FilterInstance instance, Tasklist tasklist, FilterType type) {
        FilterInstance filterInstance = filterExecutionServiceImpl.createDefault(tasklist.getId(), tasklist.getAssigneeId(), type);
        filterInstance.setTasklist(tasklist);
        return filterInstance;
    }

    public FilterInstance save(FilterInstance instance) {
        return filterExecutionServiceImpl.save(instance);
    }

}
