package de.ubt.ai4.petter.recpro.recprobackend.recpro.attribute.execution;

import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.RecproAttributeInstance;
import de.ubt.ai4.petter.recpro.lib.attributepersistence.service.AttributeInstanceExecutionService;
import de.ubt.ai4.petter.recpro.lib.attributepersistence.service.AttributeInstancePersistenceService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.attribute.execution.model.AttributeInstanceDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class AttributeExecutionService {

    private AttributeInstanceExecutionService instanceExecutionService;
    private AttributeInstancePersistenceService persistenceService;

    public List<AttributeInstanceDto> create(List<AttributeInstanceDto> instances) {
        return instances.stream().map(instance -> new AttributeInstanceDto(instance.getAttribute(), instanceExecutionService.save(instance.getInstance()))).toList();
    }

    public List<RecproAttributeInstance> getAll() {
        return persistenceService.getAll();
    }

    public List<RecproAttributeInstance> getByProcessInstanceIds(List<String> processInstanceIds) {
        return persistenceService.getByProcessInstanceIds(processInstanceIds);
    }
}
