package de.ubt.ai4.petter.recpro.recprobackend.auth;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final String E_MAIL = "email";
    private final String FIRST_NAME = "given_name";
    private final String LAST_NAME = "family_name";

    public User getCurrentUser() {
        JwtAuthenticationToken token = this.getToken();

        User user = new User();
        user.setFirstName(this.getTokenAttribute(FIRST_NAME, token));
        user.setLastName(this.getTokenAttribute(LAST_NAME, token));
        user.setId(token.getName());
        return user;
    }

    private JwtAuthenticationToken getToken() {
        return (JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
    }

    private String getTokenAttribute(String attributeId, JwtAuthenticationToken token) {
        return String.valueOf(token.getTokenAttributes().get(attributeId));
    }
}
