package de.ubt.ai4.petter.recpro.recprobackend.bpms.execution.process;

import de.ubt.ai4.petter.recpro.lib.bpms.execution.service.process.BpmsExecutionProcessService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/bpms/execution/process/")
@AllArgsConstructor
public class BpmsExecutionProcessController {

    private BpmsExecutionProcessService executionService;

    @PostMapping("start/{processId}")
    public void start(@PathVariable String processId) {
        executionService.startProcess(processId);
    }
}
