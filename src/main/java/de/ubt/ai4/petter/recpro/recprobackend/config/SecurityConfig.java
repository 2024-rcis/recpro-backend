package de.ubt.ai4.petter.recpro.recprobackend.config;

import de.ubt.ai4.petter.recpro.recprobackend.auth.ErrorHandler;
import de.ubt.ai4.petter.recpro.recprobackend.auth.KeycloakRoleConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    private final ErrorHandler errorHandler;

    public SecurityConfig(final ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    @Bean
    protected SecurityFilterChain securityFilterChain(final HttpSecurity http) throws Exception {
        http.cors(Customizer.withDefaults())
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(r -> r
                        .requestMatchers(
                                "/api/recpro/rating/persistence/getAll",
                                "/api/recpro/rating/persistence/getByUserIds",
                                "/api/recpro/bpm/execution/tasklist/getById/**",
                                "/api/modeling/filter/getById/**",
                                "/api/recpro/filter/execution/getByTasklistId/**",
                                "/api/recpro/bpm/execution/tasklist/getById/**",
                                "/api/recpro/attribute/execution/getAll",
                                "/api/modeling/attribute/getAll",
                                "/api/recpro/attribute/execution/getByProcessInstanceIds",
                                "/api/recpro/rating/persistence/getAll",
                                "/api/modeling/bpm/activity"
                        ).permitAll()
                        .anyRequest().authenticated())
                .exceptionHandling(e -> e.authenticationEntryPoint(errorHandler))
                .sessionManagement(s -> s.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .oauth2ResourceServer(
                        httpSecurityOAuth2ResourceServerConfigurer ->
                                httpSecurityOAuth2ResourceServerConfigurer.jwt(
                                        jwtConfigurer -> jwtConfigurer.jwtAuthenticationConverter(jwtConverter())));

        return http.build();
    }

    private Converter<Jwt, ? extends AbstractAuthenticationToken> jwtConverter() {
        final JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(new KeycloakRoleConverter());
        return jwtAuthenticationConverter;
    }
}
