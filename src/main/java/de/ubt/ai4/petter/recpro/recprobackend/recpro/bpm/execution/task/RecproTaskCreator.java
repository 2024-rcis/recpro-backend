package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.task;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTaskDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class RecproTaskCreator {
    public Task fromRecproTaskDto(RecproTaskDto dto) {
        Task task = new Task();
        task.setId(dto.getId());
        task.setAssigneeId(dto.getAssigneeId());
        task.setLikeliness(dto.getLikeliness());
        task.setTaskId(dto.getTaskId());
        task.setPosition(dto.getPosition());
        task.setState(dto.getState());
        task.setActivityId(dto.getActivity().getId());
        task.setProcessId(dto.getProcessInstance().getRecproElementId());
        task.setPriority(dto.getPriority());
        task.setCreateDate(dto.getCreateDate());
        task.setDueDate(dto.getDueDate());
        task.setEditDate(dto.getEditDate());
        task.setExecutionId(dto.getExecutionId());
        task.setRecproElementId(dto.getRecproElementId());
        task.setRecproElementInstanceId(dto.getRecproElementInstanceId());
        task.setProcessInstanceId(dto.getExecutionId());
        return task;
    }

    public List<Task> fromRecproTaskDto(List<RecproTaskDto> dtos) {
        return dtos.stream().map(this::fromRecproTaskDto).toList();
    }
}
