package de.ubt.ai4.petter.recpro.recprobackend.bpms.execution.task;

import de.ubt.ai4.petter.recpro.lib.bpms.execution.model.BpmsTask;
import de.ubt.ai4.petter.recpro.lib.bpms.execution.service.task.BpmsTaskService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/bpms/execution/task/")
@AllArgsConstructor
public class BpmsTaskController {

    private BpmsTaskService taskService;

    @GetMapping("{taskId}")
    public BpmsTask getById(@PathVariable String taskId) {
        return taskService.getById(taskId);
    }

    @PostMapping("claim/{taskId}")
    public void claimTask(@PathVariable String taskId, @RequestParam String userId) {
        taskService.claim(taskId, userId);
    }

    @PostMapping("unclaim/{taskId}")
    public void unclaimTask(@PathVariable String taskId) {
        taskService.unclaim(taskId);
    }

    @DeleteMapping("{taskId}")
    public void deleteTask(@PathVariable String taskId) {
        taskService.cancel(taskId);
    }

    @PostMapping("complete/{taskId}")
    public void completeTask(@PathVariable String taskId) {
        taskService.complete(taskId);
    }

    @PostMapping("start/{taskId}")
    public void startTask(@PathVariable String taskId) {
        taskService.start(taskId);
    }

}
