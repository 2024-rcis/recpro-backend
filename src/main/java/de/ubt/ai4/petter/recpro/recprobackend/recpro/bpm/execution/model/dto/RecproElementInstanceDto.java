package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto;

import de.ubt.ai4.petter.recpro.recprobackend.recpro.attribute.execution.model.AttributeInstanceDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecproElementInstanceDto {
    private Long id;
    private String assigneeId;
    private Double likeliness;
    private List<AttributeInstanceDto> attributes;
    private String recproElementId;
    private String recproElementInstanceId;
}
