package de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.creatorServices;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.filter.FilterInstanceCreator;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.base.FilterModelingService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.model.dto.RecproFilterInstanceDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RecproFilterInstanceDtoCreator {

    protected FilterInstanceCreator filterInstanceCreator;
    protected FilterModelingService filterModelingService;

    public RecproFilterInstanceDto fromRecproFilter(Filter filter, Long tasklistId, String assigneeId) {
        RecproFilterInstanceDto dto = new RecproFilterInstanceDto();
        dto.setFilter(filter);
        FilterInstance filterInstance = new FilterInstance(filter.getId(), tasklistId, assigneeId, filter.getFilterType());
        dto.setFilterInstance(filterInstanceCreator.create(filterInstance));
        return dto;
    }

    public RecproFilterInstanceDto fromRecproFilterInstance(FilterInstance instance) {
        RecproFilterInstanceDto dto = new RecproFilterInstanceDto();
        dto.setFilterInstance(instance);
        dto.setFilter(filterModelingService.getById(instance.getFilterId()));
        return dto;
    }
}
