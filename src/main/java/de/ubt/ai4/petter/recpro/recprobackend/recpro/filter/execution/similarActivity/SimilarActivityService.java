package de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.similarActivity;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.tasklist.TasklistService;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElementInstance.FilterBpmElementInstanceInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElementInstance.FilterBpmElementInstanceState;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.KnowledgeBasedFilterInstance;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.base.FilterModelingService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTaskDto;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.RecproFilterExecutionService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.model.dto.RecproFilterInstanceDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Objects;

@AllArgsConstructor
@Service
public class SimilarActivityService {

    private RecproFilterExecutionService filterExecutionService;
    private TasklistService tasklistService;
    private FilterModelingService recproFilterModelingServiceImpl;

    public RecproFilterInstanceDto getByProcessId(RecproTaskDto task) {
        Tasklist tasklist = tasklistService.getLatestByAssigneeId(task.getAssigneeId());
        KnowledgeBasedFilterInstance instance = (KnowledgeBasedFilterInstance) filterExecutionService.createFilter(null, tasklist, FilterType.KNOWLEDGE_BASED);

        FilterBpmElementInstanceInstance elementInstanceInstance = new FilterBpmElementInstanceInstance(null, task.getProcessInstance().getBpmsProcessInstanceId(), FilterBpmElementInstanceState.TRUE);

        instance.setElementInstances(new ArrayList<>(){{add(elementInstanceInstance);}});

        return this.callKnowledgeBasedFilter(instance);
    }

    private RecproFilterInstanceDto callKnowledgeBasedFilter(FilterInstance kbfi) {
        Filter filter = recproFilterModelingServiceImpl.getById(kbfi.getFilterId());
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(filter.getFilterUrl());
        kbfi = Objects.requireNonNull(template.postForObject(builder.toUriString(), kbfi, KnowledgeBasedFilterInstance.class));
        return new RecproFilterInstanceDto(kbfi, filter);
    }

}
