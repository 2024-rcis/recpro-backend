package de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.model.dto;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.attribute.FilterAttributeInstance;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class RecproFilterAttributeInstanceDto {
    private FilterAttributeInstance attributeInstance;
    private RecproAttribute attribute;
}
