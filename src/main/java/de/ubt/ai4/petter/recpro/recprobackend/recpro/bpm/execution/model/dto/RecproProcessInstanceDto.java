package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.ProcessInstance;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecproProcessInstanceDto extends RecproElementInstanceDto {
    String bpmsProcessInstanceId;
    String processName;

    public static RecproProcessInstanceDto fromRecproProcessInstance(ProcessInstance processInstance, String assigneeId) {
        RecproProcessInstanceDto dto = new RecproProcessInstanceDto();
        dto.setId(processInstance.getId());
        dto.setAssigneeId(assigneeId);
        dto.setLikeliness(processInstance.getLikeliness());
        dto.setBpmsProcessInstanceId(processInstance.getBpmsProcessInstanceId());
        dto.setAttributes(new ArrayList<>());
        return dto;
    }
}
