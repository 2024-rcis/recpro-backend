package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.tasklist;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTasklistDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/recpro/bpm/execution/tasklist/")
@AllArgsConstructor
public class RecproTasklistController {
    private RecproTasklistService tasklistService;
    @GetMapping("getAllTasks")
    public ResponseEntity<RecproTasklistDto> getAllTasks(@RequestHeader("X-User-ID") String assigneeId) {
        RecproTasklistDto result = tasklistService.getOrderedTasklist(assigneeId);
        return ResponseEntity.ok(result);
    }

    @GetMapping("getById/{tasklistId}")
    public ResponseEntity<Tasklist> getById(@PathVariable Long tasklistId) {
        return ResponseEntity.ok(tasklistService.getById(tasklistId));
    }

    @PostMapping("filter")
    public ResponseEntity<RecproTasklistDto> filterTasklist(@RequestBody FilterInstance filterInstance) {
        return ResponseEntity.ok(tasklistService.getOrderedTasklist(filterInstance));
    }
}
