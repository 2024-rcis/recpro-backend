package de.ubt.ai4.petter.recpro.recprobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.ubt.ai4.petter.recpro", "de.ubt.ai4.petter.recpro.lib.attribute", "de.ubt.ai4.petter.recpro.lib.attributepersistence", "de.ubt.ai4.petter.recpro.lib.ontology"})
public class RecproBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecproBackendApplication.class, args);
	}

}
