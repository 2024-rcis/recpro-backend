package de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import de.ubt.ai4.petter.recpro.lib.filter.filterexecution.service.IFilterExecutionService;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTasklistDto;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.tasklist.RecproTasklistService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.model.dto.RecproFilterInstanceDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/recpro/filter/execution/")
@AllArgsConstructor
public class RecproFilterExecutionController {

    private RecproFilterExecutionService executionService;
    private IFilterExecutionService filterExecutionServiceImpl;
    private RecproTasklistService tasklistService;
    @PostMapping("create")
    public ResponseEntity<FilterInstance> create(@RequestBody FilterInstance filter, @RequestParam Long tasklistId, @RequestHeader("X-User-ID") String assigneeId) {
        return ResponseEntity.ok(filterExecutionServiceImpl.create(filter.getFilterId(), tasklistId, assigneeId, filter.getFilterType()));
    }

    @GetMapping("createByFilterId")
    public ResponseEntity<RecproFilterInstanceDto> createByFilterId(@RequestParam String filterId, @RequestParam Long tasklistId, @RequestParam (value = "filterType", required = false) FilterType filterType, @RequestHeader("X-User-ID") String assigneeId) {
        return ResponseEntity.ok(executionService.getNewByFilterId(filterId, tasklistId, assigneeId, filterType));
    }

    @PostMapping("createByFilter")
    public ResponseEntity<RecproFilterInstanceDto> createByFilter(@RequestBody Filter filter, Long tasklistId, @RequestHeader("X-User-ID") String assigneeId) {
        return ResponseEntity.ok(executionService.getNewByFilter(filter, tasklistId, assigneeId));
    }

    @PostMapping("save")
    public ResponseEntity<RecproTasklistDto> save(@RequestBody FilterInstance instance) {
        return ResponseEntity.ok(tasklistService.getOrderedTasklist(instance));
    }

    @GetMapping("getByTasklistId/{tasklistId}")
    public ResponseEntity<FilterInstance> getByTasklistId(@PathVariable Long tasklistId, @RequestHeader("X-User-ID") String assigneeId, @RequestParam (value = "filterType", required = false) FilterType filterType) {
        return ResponseEntity.ok(executionService.getByTasklistId(tasklistId, assigneeId, filterType));
    }
}
