package de.ubt.ai4.petter.recpro.recprobackend.bpms.execution.tasklist;

import de.ubt.ai4.petter.recpro.lib.bpms.execution.model.BpmsTasklist;
import de.ubt.ai4.petter.recpro.lib.bpms.execution.service.tasklist.BpmsTasklistService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/bpms/execution/tasklist/")
@AllArgsConstructor
public class BpmsTasklistController {

    private BpmsTasklistService service;

    @GetMapping("get")
    public ResponseEntity<BpmsTasklist> getTasklist(@RequestParam String assigneeId) {
        return ResponseEntity.ok(service.getTasklist(assigneeId));
    }

    @GetMapping("getByAssignee")
    public ResponseEntity<BpmsTasklist> getTasklistByAssignee(@RequestParam String assigneeId) {
        return ResponseEntity.ok(service.getTasklistByAssignee(assigneeId));
    }

}
