package de.ubt.ai4.petter.recpro.recprobackend.recpro.attribute.execution.model;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.RecproAttributeInstance;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AttributeInstanceDto {
    private RecproAttribute attribute;
    private RecproAttributeInstance instance;
}
