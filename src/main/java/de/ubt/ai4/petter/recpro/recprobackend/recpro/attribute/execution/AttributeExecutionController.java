package de.ubt.ai4.petter.recpro.recprobackend.recpro.attribute.execution;

import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.RecproAttributeInstance;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.attribute.execution.model.AttributeInstanceDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/recpro/attribute/execution/")
@AllArgsConstructor
public class AttributeExecutionController {

    private AttributeExecutionService executionService;
    @PostMapping("createAttribute")
    public void createAttribute(@RequestBody RecproAttributeInstance attributeInstance) {
        // TODO document why this method is empty
    }

    @PostMapping("create")
    public ResponseEntity<List<AttributeInstanceDto>> createAttributes(@RequestBody List<AttributeInstanceDto> attributeInstanceDtos) {
        return ResponseEntity.ok(executionService.create(attributeInstanceDtos));
    }

    @GetMapping("getAll")
    public ResponseEntity<List<RecproAttributeInstance>> getAll() {
        return ResponseEntity.ok(executionService.getAll());
    }

    @GetMapping("getByProcessInstanceIds")
    public ResponseEntity<List<RecproAttributeInstance>> getByProcessInstanceIds(@RequestParam List<String> processInstanceIds) {
        return ResponseEntity.ok(executionService.getByProcessInstanceIds(processInstanceIds));
    }

    @GetMapping("temperature")
    public ResponseEntity<Double> getTemperature() {
        return ResponseEntity.ok(20.0);
    }

    @GetMapping("customer")
    public ResponseEntity<String> getCustomer() {
        return ResponseEntity.ok("CUSTOMER TEST");
    }

    @GetMapping("boolean")
    public ResponseEntity<Boolean> getBoolean() {
        return ResponseEntity.ok(false);
    }
}
