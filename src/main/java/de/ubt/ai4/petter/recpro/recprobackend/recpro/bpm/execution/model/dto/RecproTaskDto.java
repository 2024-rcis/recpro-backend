package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Activity;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.TaskState;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecproTaskDto extends RecproElementInstanceDto {
    private String taskId;
    private int position = -1;
    private TaskState state;
    private Activity activity;
    private RecproProcessInstanceDto processInstance;
    private int priority;
    private Instant createDate;
    private Instant editDate;
    private Instant dueDate;
    private String executionId;
    private boolean visible;
}
